
import 'package:flutter/material.dart';
import 'package:flutter_esdm/laphar/pages/auth/login_page.dart';
import 'package:flutter_esdm/laphar/pages/history/history_page.dart';
import 'package:flutter_esdm/laphar/pages/home/home_page.dart';
import 'package:flutter_esdm/laphar/pages/splash/splash_page.dart';
import 'package:flutter_esdm/laphar/pages/update_berita/update_berita_page.dart';
import 'package:flutter_esdm/laphar/pages/update_harian_esdm/update_harian_esdm_page.dart';
import 'package:flutter_esdm/laphar/pages/update_kegeologian/update_kegeologian_page.dart';
import 'package:flutter_esdm/portal_page.dart';
import 'package:flutter_esdm/heesi/HeesiPageView.dart';
import 'package:flutter_esdm/heesi/list_fiture.dart';

const String laphar = '/laphar';
const String heesi = '/heesi';
const String magma = '/magma';
const String georima = '/georima';
const String modi = '/modi';

Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  '/': (BuildContext context) => new SplashPage(),
  '/login': (BuildContext context) => new LoginPage(),
  '/portal': (BuildContext context) => new PortalPage(),
  laphar : (BuildContext context) => new HomePage(),
  '/update_harian_esdm': (BuildContext context) => new UpdateHarianESDMPage(),
  '/update_kegeologian': (BuildContext context) => new UpdateKegeologianPage(),
  '/update_berita': (BuildContext context) => new UpdateBeritaPage(),
  '/history': (BuildContext context) => new HistoryPage(),
  heesi : (BuildContext context) => new HeesiPageView(),
  'list_fiture_heesi': (BuildContext context) => new ListFiture()
};
