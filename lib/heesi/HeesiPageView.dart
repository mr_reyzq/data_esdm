import 'package:flutter/material.dart';
import 'package:flutter_esdm/laphar/utils/uidata.dart';
import 'package:flutter_esdm/heesi/list_fiture.dart';

class HeesiPageView extends StatefulWidget {
  @override
  _HeesiPageViewState createState() => new _HeesiPageViewState();
}

class _HeesiPageViewState extends State<HeesiPageView> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return new Scaffold(
      body: Stack(
        children: <Widget>[
          SafeArea(
            child: new Image.asset(
              'assets/background_heesi.jpg',
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            ),
          ),
          Positioned(
            left: 10,
            top: 40,
            child: Container(
              width: 50,
              child: Opacity(
                opacity: 0.0,
                child: RaisedButton(
                  padding: EdgeInsets.all(0),
                  color: UIData.colorYellow,
                  child: Image(image: AssetImage("assets/back.png"),),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ListFiture()),
                      );
                    },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
