import 'package:flutter/material.dart';
import 'package:flutter_esdm/heesi/supply_demand/pes_by_resources/PesByResourcesPage.dart';
import 'package:flutter_esdm/heesi/supply_demand/fec_by_sector/FecBySectorPage.dart';
import 'package:flutter_esdm/heesi/supply_demand/fec_by_type/FecByTypePage.dart';

class SupplyDemandPage extends StatefulWidget {
  @override
  _SupplyDemandPageState createState() => _SupplyDemandPageState();
}

class _SupplyDemandPageState extends State<SupplyDemandPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text("Primary Energy Supply by Sources"),
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PesByResourcesPage()),
              )
          ),
          ListTile(
            title: Text("Final Energy Consumption by Sector"),
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => FecBySectorPage()),
              )
          ),
          ListTile(
            title: Text("Final Consumption by Type"),
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => FecByTypePage()),
              )
          ),
        ],
      ),
    );
  }
}