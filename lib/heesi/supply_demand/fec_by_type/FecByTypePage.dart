import 'dart:async';
import 'dart:convert';
import 'package:charts_flutter/flutter.dart';

import 'package:json_table/json_table.dart';
import 'package:json_table/json_table_column.dart';

import 'package:flutter/src/painting/basic_types.dart';

import 'dart:math';
import 'package:charts_flutter/src/text_element.dart';
import 'package:charts_flutter/src/text_style.dart' as style;
import 'package:flutter_esdm/heesi/global_function/GlobalFunction.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:charts_flutter/flutter.dart' as charts;

class FecByTypePage extends StatefulWidget {
  @override
  _FecByTypePageState createState() => _FecByTypePageState();
}

String username = "admin";
String password = "1234";
String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));

class _FecByTypePageState extends State<FecByTypePage> {
  List data;
  Timer timer;
  List<JsonTableColumn> columns;

  makeRequest() async {
    var response = await http.get(
      "http://laphar.esdm.go.id/heesi/API/getData/GetAllData?table_name=final_energy_consump_per_type",
      headers: {'Accept': 'application/json', 'authorization': basicAuth},
    );

    setState(() {
      data = json.decode(response.body);
    });
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 2), (t) => makeRequest());
    columns = [
    JsonTableColumn('final_energy_consump_per_type_id', label: 'ID'),
//      JsonTableColumn('year', label: 'Year'),
    JsonTableColumn('biomass', label: 'Biomass', valueBuilder: SeperatorRibu),
    JsonTableColumn('coal', label: 'Coal', valueBuilder: SeperatorRibu),
    JsonTableColumn('natural_gas', label: 'Natural Gas', valueBuilder: SeperatorRibu),
    JsonTableColumn('fuel', label: 'Fuel', valueBuilder: SeperatorRibu),
    JsonTableColumn('bio_gasoil', label: 'Bio Gas Oil', valueBuilder: SeperatorRibu),
    JsonTableColumn('biogas', label: 'Biogas', valueBuilder: SeperatorRibu),
    JsonTableColumn('briquette', label: 'Briquette', valueBuilder: SeperatorRibu),
    JsonTableColumn('lpg', label: 'LPG', valueBuilder: SeperatorRibu),
    JsonTableColumn('electricity', label: 'Electricity', valueBuilder: SeperatorRibu),
    JsonTableColumn('total', label: 'Total', valueBuilder: SeperatorRibu),
    ];
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Final Energy Consumption by Sector')),
      body: Center(
        child: data == null
            ? CircularProgressIndicator()
            : Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          color: Colors.white,
          child: Column(children: <Widget>[
            Expanded(
                child: SafeArea(
                  child: createChart(),
                )),
            Expanded(
              child: JsonTable(data, columns: columns),
            ),
          ]),
        ),
      ),
    );
  }

  charts.Series<LiveWerkzeuge, String> createSeries(String id, int i) {
    return charts.Series<LiveWerkzeuge, String>(
      id: id,
      domainFn: (LiveWerkzeuge wear, _) => wear.wsp,
      measureFn: (LiveWerkzeuge wear, _) => wear.belastung,
      data: [
        LiveWerkzeuge('Biomass', double.parse(data[i]['biomass'] ?? "0")),
        LiveWerkzeuge('Coal', double.parse(data[i]['coal'] ?? "0")),
        LiveWerkzeuge('Natural Gas', double.parse(data[i]['natural_gas'] ?? "0")),
        LiveWerkzeuge('Fuel', double.parse(data[i]['fuel'] ?? "0")),
        LiveWerkzeuge('Bio Gas Oil', double.parse(data[i]['bio_gasoil']?? "0")),
        LiveWerkzeuge('Biogas', double.parse(data[i]['biogas'] ?? "0")),
        LiveWerkzeuge('Briquette', double.parse(data[i]['briquette'] ?? "0")),
        LiveWerkzeuge('LPG', double.parse(data[i]['lpg']?? "0")),
        LiveWerkzeuge('Electricity', double.parse(data[i]['electricity'] ?? "0")),
        LiveWerkzeuge('Total', double.parse(data[i]['total'] ?? "0")),
      ],
    );
  }

  Widget createChart() {
    List<charts.Series<LiveWerkzeuge, String>> seriesList = [];

    for (int i = 0; i < data.length; i++) {
      String id = 'share_of_primary_energy_supply_id${i + 1}';
      seriesList.add(createSeries(id, i));
    }

    return new charts.BarChart(
      seriesList,
      barGroupingType: charts.BarGroupingType.grouped,
      animate: true,
      animationDuration: Duration(milliseconds: 500),
      domainAxis: new charts.OrdinalAxisSpec(
        viewport: new charts.OrdinalViewport('2019', 1),
      ),
      behaviors: [
        new charts.SlidingViewport(),
        new charts.PanAndZoomBehavior(),
        LinePointHighlighter(symbolRenderer: CustomCircleSymbolRenderer())
      ],
      selectionModels: [
        SelectionModelConfig(changedListener: (SelectionModel model) {
          if (model.hasDatumSelection)
            setState(() {
              CustomCircleSymbolRenderer.selectedValue = model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index);
              print(model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index));
            });
        })
      ],
    );
  }
}

class CustomCircleSymbolRenderer extends CircleSymbolRenderer {
  static double selectedValue;

  @override
  void paint(ChartCanvas canvas, Rectangle<num> bounds,
      {List<int> dashPattern,
        Color fillColor,
        Color strokeColor,
        double strokeWidthPx}) {
    super.paint(canvas, bounds,
        dashPattern: dashPattern,
        fillColor: fillColor,
        strokeColor: strokeColor,
        strokeWidthPx: strokeWidthPx);
    canvas.drawRect(
        Rectangle(bounds.left + 5, bounds.top - 30, bounds.width + 10,
            bounds.height + 10),
        fill: Color.white);
    var textStyle = style.TextStyle();
    textStyle.color = Color.black;
    textStyle.fontSize = 35;
    canvas.drawText(TextElement(selectedValue.toString(), style: textStyle),
        (bounds.left).round(), (bounds.top - 28).round());
  }
}

class LiveWerkzeuge {
  final String wsp;
  final double belastung;

  LiveWerkzeuge(this.wsp, this.belastung);
}
