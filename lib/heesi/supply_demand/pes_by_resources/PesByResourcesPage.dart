import 'dart:async';
import 'dart:convert';
import 'package:charts_flutter/flutter.dart';

import 'package:json_table/json_table.dart';
import 'package:json_table/json_table_column.dart';

import 'package:flutter/src/painting/basic_types.dart';
import 'package:flutter_esdm/heesi/global_function/GlobalFunction.dart';
import 'dart:math';
import 'package:charts_flutter/src/text_element.dart';
import 'package:charts_flutter/src/text_style.dart' as style;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:charts_flutter/flutter.dart' as charts;

class PesByResourcesPage extends StatefulWidget {
  @override
  _PesByResourcesPageState createState() => _PesByResourcesPageState();
}

String username = "admin";
String password = "1234";
String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));

class _PesByResourcesPageState extends State<PesByResourcesPage> {
  List data;
  Timer timer;
  List<JsonTableColumn> columns;

  makeRequest() async {
    var response = await http.get(
      "http://laphar.esdm.go.id/heesi/API/getData/GetAllData?table_name=primary_energy_supply_by_source",
      headers: {'Accept': 'application/json', 'authorization': basicAuth},
    );

    setState(() {
      data = json.decode(response.body);
    });
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 2), (t) => makeRequest());
    columns = [
      JsonTableColumn('primary_energy_supply_by_source_id', label: 'ID', valueBuilder: SeperatorRibu),
//      JsonTableColumn('year', label: 'Year'),
      JsonTableColumn('coal', label: 'Coal', valueBuilder: SeperatorRibu),
      JsonTableColumn('crude_oil_product', label: 'Crude Oil Product', valueBuilder: SeperatorRibu),
      JsonTableColumn('natural_gas_product', label: 'Natural Gas Product', valueBuilder: SeperatorRibu),
      JsonTableColumn('hydro_power', label: 'Hydro Power', valueBuilder: SeperatorRibu),
      JsonTableColumn('geothermal', label: 'Geothermal', valueBuilder: SeperatorRibu),
      JsonTableColumn('solar_pp_solar_pv', label: 'Solar PP Solar PV', valueBuilder: SeperatorRibu),
      JsonTableColumn('wind_pp', label: 'Wind PP', valueBuilder: SeperatorRibu),
      JsonTableColumn('other_renewables_pp', label: 'Other Renewables PP', valueBuilder: SeperatorRibu),
      JsonTableColumn('public_street_lighting_ltshe', label: 'Public Street Lighting LTSHE', valueBuilder: SeperatorRibu),
      JsonTableColumn('biomass', label: 'Biomass', valueBuilder: SeperatorRibu),
      JsonTableColumn('biofuel', label: 'Biofuel', valueBuilder: SeperatorRibu),
      JsonTableColumn('biogas', label: 'Biogas', valueBuilder: SeperatorRibu),
      JsonTableColumn('total', label: 'Total', valueBuilder: SeperatorRibu),
    ];
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Primary Energy Supply by Sources')),
      body: Center(
        child: data == null
            ? CircularProgressIndicator()
            : Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          color: Colors.white,
          child: Column(children: <Widget>[
            Expanded(
                child: SafeArea(
                  child: createChart(),
                )),
            Expanded(
              child: JsonTable(data, columns: columns),
            ),
          ]),
        ),
      ),
    );
  }

  charts.Series<LiveWerkzeuge, String> createSeries(String id, int i) {
    return charts.Series<LiveWerkzeuge, String>(
      id: id,
      domainFn: (LiveWerkzeuge wear, _) => wear.wsp,
      measureFn: (LiveWerkzeuge wear, _) => wear.belastung,
      data: [
        LiveWerkzeuge('Coal', double.parse(data[i]['coal'] ?? "0")),
        LiveWerkzeuge('Crude Oil Product', double.parse(data[i]['crude_oil_product'] ?? "0")),
        LiveWerkzeuge('Natural Gas Product', double.parse(data[i]['natural_gas_product'] ?? "0")),
        LiveWerkzeuge('Hydro Power', double.parse(data[i]['hydro_power'] ?? "0")),
        LiveWerkzeuge('Geothermal', double.parse(data[i]['geothermal']?? "0")),
        LiveWerkzeuge('Solar PP Solar PV', double.parse(data[i]['solar_pp_solar_pv'] ?? "0")),
        LiveWerkzeuge('Wind PP', double.parse(data[i]['wind_pp'] ?? "0")),
        LiveWerkzeuge('Other Renewables PP', double.parse(data[i]['other_renewables_pp']?? "0")),
        LiveWerkzeuge('Public Street Lighting LTSHE', double.parse(data[i]['public_street_lighting_ltshe']?? "0")),
        LiveWerkzeuge('Biomass', double.parse(data[i]['biomass'] ?? "0")),
        LiveWerkzeuge('Biofuel', double.parse(data[i]['biofuel'] ?? "0")),
        LiveWerkzeuge('Biogas', double.parse(data[i]['biogas'] ?? "0")),
        LiveWerkzeuge('Total', double.parse(data[i]['total'] ?? "0")),
      ],
    );
  }

  Widget createChart() {
    List<charts.Series<LiveWerkzeuge, String>> seriesList = [];

    for (int i = 0; i < data.length; i++) {
      String id = 'share_of_primary_energy_supply_id${i + 1}';
      seriesList.add(createSeries(id, i));
    }

    return new charts.BarChart(
      seriesList,
      barGroupingType: charts.BarGroupingType.grouped,
      animate: true,
      animationDuration: Duration(milliseconds: 500),
      domainAxis: new charts.OrdinalAxisSpec(
        viewport: new charts.OrdinalViewport('2019', 1),
      ),
      behaviors: [
        new charts.SlidingViewport(),
        new charts.PanAndZoomBehavior(),
        LinePointHighlighter(symbolRenderer: CustomCircleSymbolRenderer())
      ],
      selectionModels: [
        SelectionModelConfig(changedListener: (SelectionModel model) {
          if (model.hasDatumSelection)
            setState(() {
              CustomCircleSymbolRenderer.selectedValue = model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index);
              print(model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index));
            });
        })
      ],
    );
  }
}

class CustomCircleSymbolRenderer extends CircleSymbolRenderer {
  static double selectedValue;

  @override
  void paint(ChartCanvas canvas, Rectangle<num> bounds,
      {List<int> dashPattern,
        Color fillColor,
        Color strokeColor,
        double strokeWidthPx}) {
    super.paint(canvas, bounds,
        dashPattern: dashPattern,
        fillColor: fillColor,
        strokeColor: strokeColor,
        strokeWidthPx: strokeWidthPx);
    canvas.drawRect(
        Rectangle(bounds.left + 5, bounds.top - 30, bounds.width + 10,
            bounds.height + 10),
        fill: Color.white);
    var textStyle = style.TextStyle();
    textStyle.color = Color.black;
    textStyle.fontSize = 35;
    canvas.drawText(TextElement(selectedValue.toString(), style: textStyle),
        (bounds.left).round(), (bounds.top - 28).round());
  }
}

class LiveWerkzeuge {
  final String wsp;
  final double belastung;

  LiveWerkzeuge(this.wsp, this.belastung);
}
