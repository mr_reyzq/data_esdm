import 'dart:async';
import 'dart:convert';
import 'package:charts_flutter/flutter.dart';

import 'package:json_table/json_table.dart';
import 'package:json_table/json_table_column.dart';

import 'package:flutter/src/painting/basic_types.dart';

import 'dart:math';
import 'package:charts_flutter/src/text_element.dart';
import 'package:charts_flutter/src/text_style.dart' as style;
import 'package:flutter_esdm/heesi/global_function/GlobalFunction.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:charts_flutter/flutter.dart' as charts;

class FecBySectorPage extends StatefulWidget {
  @override
  _FecBySectorPageState createState() => _FecBySectorPageState();
}

String username = "admin";
String password = "1234";
String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));

class _FecBySectorPageState extends State<FecBySectorPage> {
  List data;
  Timer timer;
  List<JsonTableColumn> columns;

  makeRequest() async {
    var response = await http.get(
      "http://laphar.esdm.go.id/heesi/API/getData/GetAllData?table_name=final_energy_consump_per_sector",
      headers: {'Accept': 'application/json', 'authorization': basicAuth},
    );

    setState(() {
      data = json.decode(response.body);
    });
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 2), (t) => makeRequest());
    columns = [
      JsonTableColumn('final_energy_consump_per_sector_id', label: 'ID'),
//      JsonTableColumn('year', label: 'Year'),
      JsonTableColumn('households', label: 'Households', valueBuilder: SeperatorRibu),
      JsonTableColumn('commercial', label: 'Commercial', valueBuilder: SeperatorRibu),
      JsonTableColumn('industrial', label: 'Industrial', valueBuilder: SeperatorRibu),
      JsonTableColumn('transportation', label: 'Transportation', valueBuilder: SeperatorRibu),
      JsonTableColumn('other', label: 'Other', valueBuilder: SeperatorRibu),
      JsonTableColumn('non_energy_utilization', label: 'Non Enery Utilization', valueBuilder: SeperatorRibu),
      JsonTableColumn('final_energy_consumption', label: 'Final Energy Consumption', valueBuilder: SeperatorRibu),
    ];
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Final Energy Consumption by Sector')),
      body: Center(
        child: data == null
            ? CircularProgressIndicator()
            : Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          color: Colors.white,
          child: Column(children: <Widget>[
            Expanded(
                child: SafeArea(
                  child: createChart(),
                )),
            Expanded(
              child: JsonTable(data, columns: columns),
            ),
          ]),
        ),
      ),
    );
  }

  charts.Series<LiveWerkzeuge, String> createSeries(String id, int i) {
    return charts.Series<LiveWerkzeuge, String>(
      id: id,
      domainFn: (LiveWerkzeuge wear, _) => wear.wsp,
      measureFn: (LiveWerkzeuge wear, _) => wear.belastung,
      data: [
        LiveWerkzeuge('Households', double.parse(data[i]['households'] ?? "0")),
        LiveWerkzeuge('Commercial', double.parse(data[i]['commercial'] ?? "0")),
        LiveWerkzeuge('Industrial', double.parse(data[i]['industrial'] ?? "0")),
        LiveWerkzeuge('Transportation', double.parse(data[i]['transportation'] ?? "0")),
        LiveWerkzeuge('Other', double.parse(data[i]['other']?? "0")),
        LiveWerkzeuge('Non Enery Utilization', double.parse(data[i]['non_energy_utilization'] ?? "0")),
        LiveWerkzeuge('Final Energy Consumption', double.parse(data[i]['final_energy_consumption'] ?? "0")),
      ],
    );
  }

  Widget createChart() {
    List<charts.Series<LiveWerkzeuge, String>> seriesList = [];

    for (int i = 0; i < data.length; i++) {
      String id = 'share_of_primary_energy_supply_id${i + 1}';
      seriesList.add(createSeries(id, i));
    }

    return new charts.BarChart(
      seriesList,
      barGroupingType: charts.BarGroupingType.grouped,
      animate: true,
      animationDuration: Duration(milliseconds: 500),
      domainAxis: new charts.OrdinalAxisSpec(
        viewport: new charts.OrdinalViewport('2019', 1),
      ),
      behaviors: [
        new charts.SlidingViewport(),
        new charts.PanAndZoomBehavior(),
        LinePointHighlighter(symbolRenderer: CustomCircleSymbolRenderer())
      ],
      selectionModels: [
        SelectionModelConfig(changedListener: (SelectionModel model) {
          if (model.hasDatumSelection)
            setState(() {
              CustomCircleSymbolRenderer.selectedValue = model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index);
              print(model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index));
            });
        })
      ],
    );
  }
}

class CustomCircleSymbolRenderer extends CircleSymbolRenderer {
  static double selectedValue;

  @override
  void paint(ChartCanvas canvas, Rectangle<num> bounds,
      {List<int> dashPattern,
        Color fillColor,
        Color strokeColor,
        double strokeWidthPx}) {
    super.paint(canvas, bounds,
        dashPattern: dashPattern,
        fillColor: fillColor,
        strokeColor: strokeColor,
        strokeWidthPx: strokeWidthPx);
    canvas.drawRect(
        Rectangle(bounds.left + 5, bounds.top - 30, bounds.width + 10,
            bounds.height + 10),
        fill: Color.white);
    var textStyle = style.TextStyle();
    textStyle.color = Color.black;
    textStyle.fontSize = 35;
    canvas.drawText(TextElement(selectedValue.toString(), style: textStyle),
        (bounds.left).round(), (bounds.top - 28).round());
  }
}

class LiveWerkzeuge {
  final String wsp;
  final double belastung;

  LiveWerkzeuge(this.wsp, this.belastung);
}
