import 'dart:async';
import 'dart:convert';
import 'package:charts_flutter/flutter.dart';

import 'package:json_table/json_table.dart';
import 'package:json_table/json_table_column.dart';

import 'package:flutter/src/painting/basic_types.dart';

import 'dart:math';
import 'package:charts_flutter/src/text_element.dart';
import 'package:charts_flutter/src/text_style.dart' as style;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:charts_flutter/flutter.dart' as charts;

class CommercialOriginalUnitPage extends StatefulWidget {
  @override
  _CommercialOriginalUnitPageState createState() => _CommercialOriginalUnitPageState();
}

String username = "admin";
String password = "1234";
String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));

class _CommercialOriginalUnitPageState extends State<CommercialOriginalUnitPage> {
  List data;
  Timer timer;
  List<JsonTableColumn> columns;

  makeRequest() async {
    var response = await http.get(
      "http://laphar.esdm.go.id/heesi/API/getData/GetAllData?table_name=commercial_consump_ori",
      headers: {'Accept': 'application/json', 'authorization': basicAuth},
    );

    setState(() {
      data = json.decode(response.body);
    });
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 2), (t) => makeRequest());
    columns = [
      JsonTableColumn('commercial_consump_ori_id', label: 'ID'),
      JsonTableColumn('year', label: 'Year'),
      JsonTableColumn('biomass_thousand_ton', label: 'Biomass Thousand Ton'),
      JsonTableColumn('gas_mmscf', label: 'Gas MMSCF'),
      JsonTableColumn('fuel_kerosene_kiloliter', label: 'Fuel Kerosene Kiloliter'),
      JsonTableColumn('fuel_ado_kiloliter', label: 'Fuel Ado Kiloliter'),
      JsonTableColumn('fuel_ido_kiloliter', label: 'Fuel Ido Kiloliter'),
      JsonTableColumn('fuel_total_kiloliter', label: 'Fuel Total Kiloliter'),
      JsonTableColumn('lpg_thousand_ton', label: 'LPG Thousand Ton'),
      JsonTableColumn('electricity_gwh', label: 'Electricity GWH'),
    ];
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: data == null
            ? CircularProgressIndicator()
            : Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          color: Colors.white,
          child: Column(children: <Widget>[
            Expanded(
                child: SafeArea(
                  child: createChart(),
                )),
            Expanded(
              child: JsonTable(data, columns: columns),
            ),
          ]),
        ),
      ),
    );
  }

  charts.Series<LiveWerkzeuge, String> createSeries(String id, int i) {
    return charts.Series<LiveWerkzeuge, String>(
      id: id,
      domainFn: (LiveWerkzeuge wear, _) => wear.wsp,
      measureFn: (LiveWerkzeuge wear, _) => wear.belastung,
      data: [
        LiveWerkzeuge('Biomass', double.parse(data[i]['biomass_thousand_ton'])),
        LiveWerkzeuge('Gas', double.parse(data[i]['gas_mmscf'])),
        LiveWerkzeuge('Fuel Kerosene', double.parse(data[i]['fuel_kerosene_kiloliter'])),
        LiveWerkzeuge('Fuel Ado', double.parse(data[i]['fuel_ado_kiloliter'])),
        LiveWerkzeuge('Fuel Ido', double.parse(data[i]['fuel_ido_kiloliter'])),
        LiveWerkzeuge('Fuel Total', double.parse(data[i]['fuel_total_kiloliter'] ?? "0")),
        LiveWerkzeuge('LPG', double.parse(data[i]['lpg_thousand_ton'] ?? "0")),
        LiveWerkzeuge('Electricity',
            double.parse(data[i]['electricity_gwh'] ?? "0")),
      ],
    );
  }

  Widget createChart() {
    List<charts.Series<LiveWerkzeuge, String>> seriesList = [];

    for (int i = 0; i < data.length; i++) {
      String id = 'share_of_primary_energy_supply_id${i + 1}';
      seriesList.add(createSeries(id, i));
    }

    return new charts.BarChart(
      seriesList,
      barGroupingType: charts.BarGroupingType.grouped,
      animate: true,
      animationDuration: Duration(milliseconds: 500),
      domainAxis: new charts.OrdinalAxisSpec(
        viewport: new charts.OrdinalViewport('2019', 1),
      ),
      behaviors: [
        new charts.SlidingViewport(),
        new charts.PanAndZoomBehavior(),
        LinePointHighlighter(symbolRenderer: CustomCircleSymbolRenderer())
      ],
      selectionModels: [
        SelectionModelConfig(changedListener: (SelectionModel model) {
          if (model.hasDatumSelection)
            setState(() {
              CustomCircleSymbolRenderer.selectedValue = model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index);
              print(model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index));
            });
        })
      ],
    );
  }
}

class CustomCircleSymbolRenderer extends CircleSymbolRenderer {
  static double selectedValue;

  @override
  void paint(ChartCanvas canvas, Rectangle<num> bounds,
      {List<int> dashPattern,
        Color fillColor,
        Color strokeColor,
        double strokeWidthPx}) {
    super.paint(canvas, bounds,
        dashPattern: dashPattern,
        fillColor: fillColor,
        strokeColor: strokeColor,
        strokeWidthPx: strokeWidthPx);
    canvas.drawRect(
        Rectangle(bounds.left + 5, bounds.top - 30, bounds.width + 10,
            bounds.height + 10),
        fill: Color.white);
    var textStyle = style.TextStyle();
    textStyle.color = Color.black;
    textStyle.fontSize = 35;
    canvas.drawText(TextElement(selectedValue.toString(), style: textStyle),
        (bounds.left).round(), (bounds.top - 28).round());
  }
}

class LiveWerkzeuge {
  final String wsp;
  final double belastung;

  LiveWerkzeuge(this.wsp, this.belastung);
}
