import 'package:flutter/material.dart';
import 'package:flutter_esdm/heesi/consumption/commercial_energy_unit/CommercialEnergyUnitPage.dart';
import 'package:flutter_esdm/heesi/consumption/commercial_original_unit/CommercialOriginalUnitPage.dart';
import 'package:flutter_esdm/heesi/consumption/household_energy_unit/HouseholdEnergyUnitPage.dart';
import 'package:flutter_esdm/heesi/consumption/household_original_unit/HouseholdOriginalUnitPage.dart';
import 'package:flutter_esdm/heesi/consumption/industrial_energy_unit/IndustrialEnergyUnitPage.dart';
import 'package:flutter_esdm/heesi/consumption/industrial_original_unit/IndustrialOriginalUnitPage.dart';
import 'package:flutter_esdm/heesi/consumption/other_energy_unit/OtherEnergyUnitPage.dart';
import 'package:flutter_esdm/heesi/consumption/other_original_unit/OtherOriginalUnitPage.dart';
import 'package:flutter_esdm/heesi/consumption/transportation_energy_unit/TransportationEnergyUnitPage.dart';
import 'package:flutter_esdm/heesi/consumption/transportation_original_unit/TransportationOriginalUnitPage.dart';

class ConsumptionPage extends StatefulWidget {
  @override
  _ConsumptionPageState createState() => _ConsumptionPageState();
}

class _ConsumptionPageState extends State<ConsumptionPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text("Industrial Original Unit"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => IndustrialOriginalUnitPage()),
            )
          ),
          ListTile(
            title: Text("Industrial Energy Unit"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => IndustrialEnergyUnitPage()),
            )
          ),
          ListTile(
            title: Text("Household Original Unit"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HouseholdOriginalUnitPage()),
            )
          ),
          ListTile(
            title: Text("Household Energy Unit"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HouseholdEnergyUnitPage()),
            )
          ),
          ListTile(
            title: Text("Commercial Original Unit"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CommercialOriginalUnitPage()),
            )
          ),
          ListTile(
            title: Text("Commercial Energy Unit"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CommercialEnergyUnitPage()),
            )
          ),
          ListTile(
            title: Text("Transportation Original Unit"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => TransportationOriginalUnitPage()),
            )
          ),
          ListTile(
            title: Text("Transportation Energy Unit"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => TransportationEnergyUnitPage()),
            )
          ),
          ListTile(
            title: Text("Other Original Unit"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => OtherOriginalUnitPage()),
            )
          ),
          ListTile(
            title: Text("Other Energy Unit"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => OtherEnergyUnitPage()),
            )
          ),
        ],
      ),
    );
  }
}