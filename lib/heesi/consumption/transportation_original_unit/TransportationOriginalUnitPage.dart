import 'dart:async';
import 'dart:convert';
import 'package:charts_flutter/flutter.dart';

import 'package:json_table/json_table.dart';
import 'package:json_table/json_table_column.dart';

import 'package:flutter/src/painting/basic_types.dart';

import 'dart:math';
import 'package:charts_flutter/src/text_element.dart';
import 'package:charts_flutter/src/text_style.dart' as style;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:charts_flutter/flutter.dart' as charts;

class TransportationOriginalUnitPage extends StatefulWidget {
  @override
  _TransportationOriginalUnitPageState createState() => _TransportationOriginalUnitPageState();
}

String username = "admin";
String password = "1234";
String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));

class _TransportationOriginalUnitPageState extends State<TransportationOriginalUnitPage> {
  List data;
  Timer timer;
  List<JsonTableColumn> columns;

  makeRequest() async {
    var response = await http.get(
      "http://laphar.esdm.go.id/heesi/API/getData/GetAllData?table_name=transport_consump_ori",
      headers: {'Accept': 'application/json', 'authorization': basicAuth},
    );

    setState(() {
      data = json.decode(response.body);
    });
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 2), (t) => makeRequest());
    columns = [
      JsonTableColumn('Transport_Consump_Ori_id', label: 'ID'),
      JsonTableColumn('year', label: 'Year'),
      JsonTableColumn('gas_mmscf', label: 'Gas'),
      JsonTableColumn('fuel_avgas_kl', label: 'Fuel Avgas'),
      JsonTableColumn('fuel_avtur_kl', label: 'Fuel Avtur'),
      JsonTableColumn('fuel_ron88_kl', label: 'Fuel RON 88'),
      JsonTableColumn('fuel_ron92_kl', label: 'Fuel RON 92'),
      JsonTableColumn('fuel_ron9598100_kl', label: 'Fuel RON 9598100'),
      JsonTableColumn('fuel_ron90_kl', label: 'Fuel RON 90'),
      JsonTableColumn('fuel_solar51_kl', label: 'Fuel Solar 51'),
      JsonTableColumn('fuel_solar53_kl', label: 'Fuel Solar 53'),
      JsonTableColumn('fuel_kerosene_kl', label: 'Fuel Kerosene'),
      JsonTableColumn('fuel_ado_kl', label: 'Fuel Ado'),
      JsonTableColumn('fuel_ido_kl', label: 'Fuel Ido'),
      JsonTableColumn('fuel_fuel_oil_kl', label: 'Fuel Fuel Oil'),
      JsonTableColumn('fuel_bioron88_kl', label: 'Fuel Bioron 88'),
      JsonTableColumn('fuel_bioron92_kl', label: 'Fuel Bioron 92'),
      JsonTableColumn('fuel_biosolar_kl', label: 'Fuel Biosolar'),
      JsonTableColumn('total_biofuel', label: 'Total BioFuel'),
      JsonTableColumn('electricity_gwh', label: 'Electricity GWH'),
    ];
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: data == null
            ? CircularProgressIndicator()
            : Container(
          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          color: Colors.white,
          child: Column(children: <Widget>[
            Expanded(
                child: SafeArea(
                  child: createChart(),
                )),
            Expanded(
              child: JsonTable(data, columns: columns),
            ),
          ]),
        ),
      ),
    );
  }

  charts.Series<LiveWerkzeuge, String> createSeries(String id, int i) {
    return charts.Series<LiveWerkzeuge, String>(
      id: id,
      domainFn: (LiveWerkzeuge wear, _) => wear.wsp,
      measureFn: (LiveWerkzeuge wear, _) => wear.belastung,
      data: [
        LiveWerkzeuge('Gas', double.parse(data[i]['gas_mmscf'] ?? "0")),
        LiveWerkzeuge('Fuel Avgas', double.parse(data[i]['fuel_avgas_kl'] ?? "0")),
        LiveWerkzeuge('Fuel Avtur', double.parse(data[i]['fuel_avtur_kl'] ?? "0")),
        LiveWerkzeuge('Fuel RON 88', double.parse(data[i]['fuel_ron88_kl'] ?? "0")),
        LiveWerkzeuge('Fuel RON 92', double.parse(data[i]['fuel_ron92_kl']?? "0")),
        LiveWerkzeuge('Fuel RON 9598100', double.parse(data[i]['fuel_ron9598100_kl'] ?? "0")),
        LiveWerkzeuge('Fuel RON 90', double.parse(data[i]['fuel_ron90_kl'] ?? "0")),
        LiveWerkzeuge('Fuel Solar 51', double.parse(data[i]['fuel_solar51_kl'] ?? "0")),
        LiveWerkzeuge('Fuel Solar 53', double.parse(data[i]['fuel_solar53_kl'] ?? "0")),
        LiveWerkzeuge('Fuel Kerosene', double.parse(data[i]['fuel_kerosene_kl'] ?? "0")),
        LiveWerkzeuge('Fuel Ado', double.parse(data[i]['fuel_ado_kl']?? "0")),
        LiveWerkzeuge('Fuel Ido', double.parse(data[i]['fuel_ido_kl'] ?? "0")),
        LiveWerkzeuge('Fuel Fuel Oil', double.parse(data[i]['fuel_fuel_oil_kl'] ?? "0")),
        LiveWerkzeuge('Fuel Bioron 88', double.parse(data[i]['fuel_bioron88_kl'] ?? "0")),
        LiveWerkzeuge('Fuel Bioron 92', double.parse(data[i]['fuel_bioron92_kl'] ?? "0")),
        LiveWerkzeuge('Fuel Biosolar', double.parse(data[i]['fuel_biosolar_kl'] ?? "0")),
        LiveWerkzeuge('Total BioFuel', double.parse(data[i]['total_biofuel'] ?? "0")),
        LiveWerkzeuge('Electricity GWH', double.parse(data[i]['electricity_gwh'] ?? "0")),
      ],
    );
  }

  Widget createChart() {
    List<charts.Series<LiveWerkzeuge, String>> seriesList = [];

    for (int i = 0; i < data.length; i++) {
      String id = 'share_of_primary_energy_supply_id${i + 1}';
      seriesList.add(createSeries(id, i));
    }

    return new charts.BarChart(
      seriesList,
      barGroupingType: charts.BarGroupingType.grouped,
      animate: true,
      animationDuration: Duration(milliseconds: 500),
      domainAxis: new charts.OrdinalAxisSpec(
        viewport: new charts.OrdinalViewport('2019', 1),
      ),
      behaviors: [
        new charts.SlidingViewport(),
        new charts.PanAndZoomBehavior(),
        LinePointHighlighter(symbolRenderer: CustomCircleSymbolRenderer())
      ],
      selectionModels: [
        SelectionModelConfig(changedListener: (SelectionModel model) {
          if (model.hasDatumSelection)
            setState(() {
              CustomCircleSymbolRenderer.selectedValue = model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index);
              print(model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index));
            });
        })
      ],
    );
  }
}

class CustomCircleSymbolRenderer extends CircleSymbolRenderer {
  static double selectedValue;

  @override
  void paint(ChartCanvas canvas, Rectangle<num> bounds,
      {List<int> dashPattern,
        Color fillColor,
        Color strokeColor,
        double strokeWidthPx}) {
    super.paint(canvas, bounds,
        dashPattern: dashPattern,
        fillColor: fillColor,
        strokeColor: strokeColor,
        strokeWidthPx: strokeWidthPx);
    canvas.drawRect(
        Rectangle(bounds.left + 5, bounds.top - 30, bounds.width + 10,
            bounds.height + 10),
        fill: Color.white);
    var textStyle = style.TextStyle();
    textStyle.color = Color.black;
    textStyle.fontSize = 35;
    canvas.drawText(TextElement(selectedValue.toString(), style: textStyle),
        (bounds.left).round(), (bounds.top - 28).round());
  }
}

class LiveWerkzeuge {
  final String wsp;
  final double belastung;

  LiveWerkzeuge(this.wsp, this.belastung);
}
