import 'dart:async';
import 'dart:convert';
import 'package:charts_flutter/flutter.dart';

import 'package:json_table/json_table.dart';
import 'package:json_table/json_table_column.dart';

import 'package:flutter/src/painting/basic_types.dart';

import 'package:intl/intl.dart';

import 'dart:math';
import 'package:charts_flutter/src/text_element.dart';
import 'package:charts_flutter/src/text_style.dart' as style;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:charts_flutter/flutter.dart' as charts;

import 'package:flutter_charts/flutter_charts.dart';

class SopEnergySupplyPage extends StatefulWidget {
  @override
  _SopEnergySupplyPageState createState() => _SopEnergySupplyPageState();
}

String username = "admin";
String password = "1234";
String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));

class _SopEnergySupplyPageState extends State<SopEnergySupplyPage> {
  List data;
  Timer timer;
  List<JsonTableColumn> columns;
  List<ChartData> dataRows;

  makeRequest() async {
    var response = await http.get(
      "http://laphar.esdm.go.id/heesi/API/getData/GetAllData?table_name=share_of_primary_energy_supply",
      headers: {'Accept': 'application/json', 'authorization': basicAuth},
    );

    setState(() {
      data = json.decode(response.body);
    });
  }

  String formatDOB(value) {
    final formatter = new NumberFormat("#,###");
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 2), (t) => makeRequest());
    columns = [
      JsonTableColumn('share_of_primary_energy_supply_id',
          defaultValue: "-", label: 'No'),
//      JsonTableColumn('year', defaultValue: "-",label: 'Year'),
      JsonTableColumn('oil', defaultValue: "-", label: 'Oil', valueBuilder: formatDOB),
      JsonTableColumn('coal', defaultValue: "-", label: 'Coal', valueBuilder: formatDOB),
      JsonTableColumn('gas', defaultValue: "-", label: 'Gas', valueBuilder: formatDOB),
      JsonTableColumn('hydropower', defaultValue: "-", label: 'Hydropower', valueBuilder: formatDOB),
      JsonTableColumn('geothermal', defaultValue: "-", label: 'Geothermal', valueBuilder: formatDOB),
      JsonTableColumn('solar', defaultValue: "-", label: 'Solar', valueBuilder: formatDOB),
      JsonTableColumn('wind', defaultValue: "-", label: 'Wind', valueBuilder: formatDOB),
      JsonTableColumn('other_renewables',
          defaultValue: "-", label: 'Other Renewables', valueBuilder: formatDOB),
      JsonTableColumn('biofuel', defaultValue: "-", label: 'Biofuel', valueBuilder: formatDOB),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(title: const Text('Share of Primary Energy Supply')),
      body: Center(
        child: data == null
            ? CircularProgressIndicator()
            : Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                child: new Column(children: <Widget>[
                  Expanded(
                    child: new Row(
                        // this stretch carries | expansion to <--> Expanded children
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          new Expanded(
                              child: SafeArea(
                            child: createChart(),
                          ))
                        ]),
                  ),
                  Expanded(
                    child: JsonTable(data, columns: columns),
                  ),
                ]),
              ),
      ),
    );
  }

  charts.Series<LiveWerkzeuge, String> createSeries(String id, int i) {
    return charts.Series<LiveWerkzeuge, String>(
      id: id,
      domainFn: (LiveWerkzeuge wear, _) => wear.wsp,
      measureFn: (LiveWerkzeuge wear, _) => wear.belastung,
      data: [
        LiveWerkzeuge('Oil  ', double.parse(data[i]['oil'])),
        LiveWerkzeuge('Coal  ', double.parse(data[i]['coal'])),
        LiveWerkzeuge('Gas  ', double.parse(data[i]['gas'])),
        LiveWerkzeuge('Hydropower  ', double.parse(data[i]['hydropower'])),
        LiveWerkzeuge('Geothermal  ', double.parse(data[i]['geothermal'])),
        LiveWerkzeuge('Solar ', double.parse(data[i]['solar'] ?? "0")),
        LiveWerkzeuge('Wind ', double.parse(data[i]['wind'] ?? "0")),
        LiveWerkzeuge('Other Renewables ',
            double.parse(data[i]['other_renewables'] ?? "0")),
        LiveWerkzeuge('Biofuel  ', double.parse(data[i]['biofuel'])),
      ],
    );
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  Widget createChart() {
    List<charts.Series<LiveWerkzeuge, String>> seriesList = [];

    for (int i = 0; i < data.length; i++) {
      String id = 'share_of_primary_energy_supply_id${i + 1}';
      seriesList.add(createSeries(id, i));
    }

    return new charts.BarChart(
      seriesList,
      barGroupingType: charts.BarGroupingType.grouped,
      animate: true,
      animationDuration: Duration(milliseconds: 500),
      domainAxis: new charts.OrdinalAxisSpec(
        viewport: new charts.OrdinalViewport('2019', 1),
      ),
      behaviors: [
        new charts.SlidingViewport(),
        new charts.PanAndZoomBehavior(),
        LinePointHighlighter(symbolRenderer: CustomCircleSymbolRenderer())
      ],
      selectionModels: [
        SelectionModelConfig(changedListener: (SelectionModel model) {
          if (model.hasDatumSelection)
            setState(() {
              CustomCircleSymbolRenderer.selectedValue = model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index);
              print(model.selectedSeries[0]
                  .measureFn(model.selectedDatum[0].index));
            });
        })
      ],
    );
  }
}

class CustomCircleSymbolRenderer extends CircleSymbolRenderer {
  static double selectedValue;

  @override
  void paint(ChartCanvas canvas, Rectangle<num> bounds,
      {List<int> dashPattern,
      Color fillColor,
      Color strokeColor,
      double strokeWidthPx}) {
    super.paint(canvas, bounds,
        dashPattern: dashPattern,
        fillColor: fillColor,
        strokeColor: strokeColor,
        strokeWidthPx: strokeWidthPx);
    canvas.drawRect(
        Rectangle(bounds.left + 5, bounds.top - 30, bounds.width + 10,
            bounds.height + 10),
        fill: Color.white);
    var textStyle = style.TextStyle();
    textStyle.color = Color.black;
    textStyle.fontSize = 35;
    canvas.drawText(TextElement(selectedValue.toString(), style: textStyle),
        (bounds.left).round(), (bounds.top - 28).round());
  }
}

class LiveWerkzeuge {
  final String wsp;
  final double belastung;

  LiveWerkzeuge(this.wsp, this.belastung);
}
