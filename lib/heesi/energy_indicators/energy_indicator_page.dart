import 'package:flutter/material.dart';
import 'package:flutter_esdm/heesi/energy_indicators/sop_energy_supply/SopEnergySupplyPage.dart';
import 'package:toast/toast.dart';

class EnergyIndicatorPage extends StatefulWidget {
  @override
  _EnergyIndicatorPageState createState() => _EnergyIndicatorPageState();
}

class _EnergyIndicatorPageState extends State<EnergyIndicatorPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text("Share of Primary Energy Supply"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SopEnergySupplyPage()),
            )
          ),
//          ListTile(
//            title: Text("Comparison of Primary Energy Intensity in Countries"),
//              onTap: () => Navigator.push(
//                context,
//                MaterialPageRoute(builder: (context) => SampleChart()),
//              )
//          ),
          ListTile(
            title: Text("Intensity of Final Energy Consumption per Capita"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
        ],
      ),
    );
  }
}