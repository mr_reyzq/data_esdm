import 'dart:async';
import 'dart:convert';
import 'package:json_table/json_table.dart';
import 'package:json_table/json_table_column.dart';
import 'package:flutter_esdm/heesi/global_function/GlobalFunction.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EnergyBalancePage extends StatefulWidget {
  @override
  _EnergyBalancePageState createState() => _EnergyBalancePageState();
}

String username = "admin";
String password = "1234";
String basicAuth = 'Basic ' + base64Encode(utf8.encode('$username:$password'));
String satuan = 'BPOD';

class _EnergyBalancePageState extends State<EnergyBalancePage> {
  List data;
  Timer timer;
  List<JsonTableColumn> columns;
  final formatCurrency = new NumberFormat.simpleCurrency();

  makeRequest() async {
    var response = await http.get(
      "http://laphar.esdm.go.id/heesi/API/getData/GetAllData?table_name=ebt",
      headers: {'Accept': 'application/json', 'authorization': basicAuth},
    );

    setState(() {
      data = json.decode(response.body);
    });
  }

  @override
  void initState() {
    super.initState();
    timer = new Timer.periodic(new Duration(seconds: 2), (t) => makeRequest());
    columns = [
      JsonTableColumn('keterangan', label: 'Keterangan', defaultValue: "-"),
      JsonTableColumn('hydro_power', label: 'Hydro Power', valueBuilder: SeperatorRibu, defaultValue: "-"),
      JsonTableColumn('geothermal', label: 'Geothermal', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('solar_pp_solar_pv', label: 'Solar PP Solar PV', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('wind_pp', label: 'Wind PP ', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('other_renewables', label: 'Other Renewables', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('public_street_lighting',
          label: 'Public Street Lighting', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('biomass', label: 'Biomass', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('coal', label: 'Coal', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('briquette', label: 'Briquette', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('natural_gas', label: 'Natural GAS', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('crude_oil', label: 'Crude Oil', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('fuel', label: 'Fuel', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('biofuel', label: 'Biofuel', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('lpg', label: 'LPG', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('electricity', label: 'Electricity', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('lng', label: 'LNG', defaultValue: "-", valueBuilder: SeperatorRibu),
      JsonTableColumn('total', label: 'Total', defaultValue: "-", valueBuilder: SeperatorRibu),
    ];
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: const Text('Energy Balance Table')),
      body: Center(
        child: data == null
            ? CircularProgressIndicator()
            : Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                color: Colors.white,
                child: Column(children: <Widget>[
                  Expanded(
                    child:Center(
                    child: SafeArea(child: JsonTable(data, columns: columns))),
                  ),
                ]),
              ),
      ),
    );
  }

}
