import 'package:flutter/material.dart';
import 'package:flutter_esdm/heesi/supply_demand/supply_demand_page.dart';
import 'package:flutter_esdm/heesi/energy_indicators/energy_indicator_page.dart';
import 'package:flutter_esdm/heesi/consumption/consumption_page.dart';
import 'package:flutter_esdm/heesi/energy_balance_table/energy_balance.dart';
import 'package:toast/toast.dart';

class ListFiture extends StatefulWidget {
  @override
  _ListFitureState createState() => new _ListFitureState();
}

class _ListFitureState extends State<ListFiture> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text("Demography"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
          ListTile(
            title: Text("Finance Banking"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
          ListTile(
            title: Text("Energy Balances"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => EnergyBalancePage()),
            ),
          ),
          ListTile(
            title: Text("Energy Indicators"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => EnergyIndicatorPage()),
            ),
          ),
          ListTile(
            title: Text("Supply Demand"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SupplyDemandPage()),
            ),
          ),
          ListTile(
            title: Text("Prices"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
          ListTile(
            title: Text("Consumption"),
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ConsumptionPage()),
            ),
          ),
          ListTile(
            title: Text("Coal"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
          ListTile(
            title: Text("Oil"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
          ListTile(
            title: Text("Gas"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
          ListTile(
            title: Text("Electric"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
          ListTile(
            title: Text("Geothermal"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
          ListTile(
            title: Text("Biofuel"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
          ListTile(
            title: Text("Glossary"),
            onTap: () {
              Toast.show("Coming Soon", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            },
          ),
          ListTile(
            title: Text("Exit"),
            onTap: () => Navigator.pop(context, false),
          ),
        ],
      ),
    );
  }
}
