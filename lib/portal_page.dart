import 'package:flutter/material.dart';
import 'package:flutter_esdm/laphar/blocs/home/home_bloc.dart';
import 'package:flutter_esdm/laphar/utils/uidata.dart';
import 'package:toast/toast.dart';
import 'package:flutter_esdm/heesi/HeesiPageView.dart';
import 'package:flutter_esdm/laphar/pages/auth/login_page.dart';
import 'package:url_launcher/url_launcher.dart';

class PortalPage extends StatefulWidget {
  @override
  _PortalPageState createState() => _PortalPageState();
}

class _PortalPageState extends State<PortalPage> {
  HomeBloc homeBloc = HomeBloc();
  Widget btn(
      int index,
      BuildContext ctx,
      String imageName,
      String txt,
      String routeName,
      ) {
    return InkWell(
      onTap: () => homeBloc.setAsZooom(ctx, index, routeName),
      child: Card(
        color: UIData.colorYellow,
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5,
        margin: EdgeInsets.all(10),
        child: Container(
          width: MediaQuery.of(ctx).size.width / 2 - 20,
          padding: EdgeInsets.all(8),
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(ctx).size.width / 2 - 50,
                height: MediaQuery.of(ctx).size.width / 2 - 50,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.black, width: 2),
                    borderRadius: BorderRadius.circular(10)),
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    StreamBuilder(
                      initialData: [false, false, false, false],
                      stream: homeBloc.zoomStream,
                      builder: (BuildContext context,
                          AsyncSnapshot<List<bool>> snapshot) {
                        double position = 0;
                        double padding = 50;
                        if (index == 4) {
                          padding = snapshot.data.elementAt(index) ? -15 : 20;
                          return AnimatedPadding(
                              duration: Duration(milliseconds: 500),
                              padding: EdgeInsets.all(padding),
                              child: Image.asset(
                                "assets/$imageName",
                                fit: BoxFit.fill,
                                width: MediaQuery.of(ctx).size.width / 2 - 50,
                                height: MediaQuery.of(ctx).size.width / 2 - 50,
                              ));
                        } else {
                          position = snapshot.data.elementAt(index) ? -15 : 20;
                          return AnimatedPositioned(
                              duration: Duration(milliseconds: 500),
                              left: position,
                              top: position,
                              right: position,
                              bottom: position,
                              child: Image.asset(
                                "assets/$imageName",
                                fit: BoxFit.fill,
                              ));
                        }
                      },
                    )
                  ],
                ),
                // child: Image(image: AssetImage("assets/$imageName"),)
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                padding: EdgeInsets.all(5),
                width: MediaQuery.of(ctx).size.width / 2 - 50,
                decoration: BoxDecoration(
                  color: Colors.black,
                ),
                child: Text(
                  txt,
                  style: TextStyle(color: Colors.white, fontSize: 18),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: new Image.asset(
              'assets/background_portal.png',
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            ),
          ),
          Positioned(
            left: 20,
            top: 50,
            child: Container(
              width: 50,
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Card(
                    child: InkWell(
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LoginPage()),
                      ),
                      child: Image(
                        image: AssetImage('assets/i_laphar.png'),
                        height: 120.0,
                        width: 120.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Card(
                    child: InkWell(
                      onTap: () { launch('https://magma.vsi.esdm.go.id/');},
                      child: Image(
                        image: AssetImage('assets/i_magma.png'),
                        height: 120.0,
                        width: 120.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Card(
                    child: InkWell(
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => HeesiPageView()),
                      ),
                      child: Image(
                        image: AssetImage('assets/i_heesi.png'),
                        height: 120.0,
                        width: 120.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Card(
                    child: InkWell(
                      onTap: () { launch('https://georima.esdm.go.id/');},
                      child: Image(
                        image: AssetImage('assets/i_georima.png'),
                        height: 120.0,
                        width: 120.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Card(
                    child: InkWell(
                      onTap: () { launch('https://modi.minerba.esdm.go.id/');},
                      child: Image(
                        image: AssetImage('assets/i_modi.png'),
                        height: 120.0,
                        width: 120.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
