import 'package:flutter/material.dart';
import 'package:flutter_esdm/laphar/blocs/global_bloc.dart';
import 'package:flutter_esdm/laphar/blocs/update_harian_esdm/update_harian_esdm_bloc.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinBeritaOpecHarian.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinHargaBbAcuan.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinHargaBbm.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinHargaMineralAcuan.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinIcp.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinLiftTb.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinProdEkuiMigas.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinProdGas.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinProdMinyak.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinSttsTl.dart';
import 'package:flutter_esdm/laphar/models/LapPusdatinCatatan.dart';
import 'package:flutter_esdm/laphar/utils/global_function.dart';
import 'package:flutter_esdm/laphar/widget/accordion_widget.dart';
import 'package:flutter_esdm/laphar/widget/card_widget.dart';
import 'package:flutter_esdm/laphar/widget/loading_widget.dart';
class UpdateHarianESDMPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    GlobalBloc globalBloc = GlobalBloc();
    GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
    globalBloc.setListExpandCount(11);
    UpdateHarianESDMBloc updateHarianESDMBloc = new UpdateHarianESDMBloc(key:_key);
    return Stack(
      children: <Widget>[
        Scaffold(
          key: _key,
          appBar: AppBar(
            // automaticallyImplyLeading: false, // Don't show the leading button
            title: Text("Update Harian ESDM",style: TextStyle(color: Colors.white),),
            backgroundColor: Colors.black,
          ),
          body: ListView(
            children: <Widget>[
              AccordionWidget(0,globalBloc,"PRODUKSI MINYAK",
                Column(
                  children: <Widget>[
                    // DatePickerWidget(onChange: (DateTime date){
                    //   updateHarianESDMBloc.onChange("lap_pusdatin_prod_minyak", date);
                    // }),
                    StreamBuilder(
                      stream: updateHarianESDMBloc.lapPusdatinProdMinyakStream,
                      builder: (context, AsyncSnapshot<LapPusdatinProdMinyak> snapshot) {
                        if (snapshot.hasData) {
                          var data = snapshot.data;
                          if (["",null].contains(data.idLaporan)) return Container();
                          return Column(
                            children: <Widget>[
                              CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                              CardWidget(
                                "Produksi Harian : ",
                                "${GlobalFunction.withNumberFormat(data.prodHarian)} BOPD(${int.parse(data.prodHarian) < int.parse(data.apbn) ? "Belum" : "Sudah"} Tercapai)",
                                statusColor: int.parse(data.prodHarian) < int.parse(data.apbn) ? Colors.red : Colors.green),
                              CardWidget(
                                "Produksi Bulanan : ",
                                "${GlobalFunction.withNumberFormat(data.prodBulanan)} BOPD(${int.parse(data.prodBulanan) < int.parse(data.apbn) ? "Belum" : "Sudah"} Tercapai)",
                                statusColor: int.parse(data.prodBulanan) < int.parse(data.apbn) ? Colors.red : Colors.green),
                              CardWidget("Produksi Tahunan : ",
                                "${GlobalFunction.withNumberFormat(data.prodTahunan)} BOPD(${int.parse(data.prodTahunan) < int.parse(data.apbn) ? "Belum" : "Sudah"} Tercapai)",
                                statusColor: int.parse(data.prodTahunan) < int.parse(data.apbn) ? Colors.red : Colors.green),
                              CardWidget("Target APBN : ","${GlobalFunction.withNumberFormat(data.apbn)} BOPD",statusColor: Colors.black,),
                            ],
                          );
                        }
                        return Container();
                      }),
                  ],
                )
              ),
              AccordionWidget(1,globalBloc,"ICP",
                Column(
                  children: <Widget>[
                    // DatePickerWidget(onChange: (DateTime date){
                    //   updateHarianESDMBloc.onChange("lap_pusdatin_icp", date);
                    // }),
                    StreamBuilder(
                      stream: updateHarianESDMBloc.lapPusdatinIcpStream,
                      builder: (context, AsyncSnapshot<LapPusdatinIcp> snapshot) {
                        if (snapshot.hasData) {
                          var data = snapshot.data;
                          if (["",null].contains(data.idLaporan)) return Container();
                          return Column(
                            children: <Widget>[
                              CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                              CardWidget("Jan : ",data.prod01+" USD/Barrel"),
                              CardWidget("Feb : ",data.prod02+" USD/Barrel"),
                              CardWidget("Mar : ",data.prod03+" USD/Barrel"),
                              CardWidget("Apr : ",data.prod04+" USD/Barrel"),
                              CardWidget("Mei : ",data.prod05+" USD/Barrel"),
                              CardWidget("Jun : ",data.prod06+" USD/Barrel"),
                              CardWidget("Jul : ",data.prod07+" USD/Barrel"),
                              CardWidget("Ags : ",data.prod08+" USD/Barrel"),
                              CardWidget("Sept : ",data.prod09+" USD/Barrel"),
                              CardWidget("Okt : ",data.prod10+" USD/Barrel"),
                              CardWidget("Nov : ",data.prod11+" USD/Barrel"),
                              CardWidget("Des : ",data.prod12+" USD/Barrel"),
                              CardWidget("Catatan : ",data.catatan+"USD/Barrel"),
                              CardWidget("Rata-Rata : ",data.rata_rata),
                            ],
                          );
                        }
                        return Container();
                      }),
                  ],
                )
              ),
              AccordionWidget(2,globalBloc,"PRODUKSI GAS",
                Column(
                  children: <Widget>[
                    // DatePickerWidget(onChange: (DateTime date){
                    //   updateHarianESDMBloc.onChange("lap_pusdatin_prod_gas", date);
                    // }),
                    StreamBuilder(
                      stream: updateHarianESDMBloc.lapPusdatinProdGasStream,
                      builder: (context, AsyncSnapshot<LapPusdatinProdGas> snapshot) {
                        if (snapshot.hasData) {
                          var data = snapshot.data;
                          if (["",null].contains(data.idLaporan)) return Container();
                          print(data.toJson());
                          return Column(
                            children: <Widget>[
                              CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                              CardWidget(
                                "Produksi Harian : ",
                                "${GlobalFunction.withNumberFormat(data.prodHarian)} MMSCFD(${int.parse(data.prodHarian) < int.parse(data.apbn) ? "Belum" : "Sudah"} Tercapai)",
                                statusColor: int.parse(data.prodHarian) < int.parse(data.apbn) ? Colors.red : Colors.green),
                              CardWidget(
                                "Produksi Bulanan : ",
                                "${GlobalFunction.withNumberFormat(data.prodBulanan)} MMSCFD(${int.parse(data.prodBulanan) < int.parse(data.apbn) ? "Belum" : "Sudah"} Tercapai)",
                                statusColor: int.parse(data.prodBulanan) < int.parse(data.apbn) ? Colors.red : Colors.green),
                              CardWidget("Produksi Tahunan : ",
                                "${GlobalFunction.withNumberFormat(data.prodTahunan)} MMSCFD(${int.parse(data.prodTahunan) < int.parse(data.apbn) ? "Belum" : "Sudah"} Tercapai)",
                                statusColor: int.parse(data.prodTahunan) < int.parse(data.apbn) ? Colors.red : Colors.green),
                              CardWidget("Target APBN : ","${GlobalFunction.withNumberFormat(data.apbn)} MMSCFD",statusColor: Colors.black,),
                            ],
                          );
                        }
                        return Container();
                      }),
                  ],
                )
              ),
              AccordionWidget(3,globalBloc,"PRODUKSI EKUIVALEN MINYAK DAN GAS",
                Column(
                  children: <Widget>[
                    // DatePickerWidget(onChange: (DateTime date){
                    //   updateHarianESDMBloc.onChange("lap_pusdatin_prod_ekui_migas", date);
                    // }),
                    StreamBuilder(
                      stream: updateHarianESDMBloc.lapPusdatinProdEkuiMigasStream,
                      builder: (context, AsyncSnapshot<LapPusdatinProdEkuiMigas> snapshot) {
                        if (snapshot.hasData) {
                          var data = snapshot.data;
                          if (["",null].contains(data.idLaporan)) return Container();
                          return Column(
                            children: <Widget>[
                              CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                              CardWidget(
                                "Produksi Harian : ",
                                "${GlobalFunction.withNumberFormat(data.prodHarian)} BOEPD(${int.parse(data.prodHarian) < int.parse(data.apbn) ? "Belum" : "Sudah"} Tercapai)",
                                statusColor: int.parse(data.prodHarian) < int.parse(data.apbn) ? Colors.red : Colors.green),
                              CardWidget(
                                "Produksi Bulanan : ",
                                "${GlobalFunction.withNumberFormat(data.prodBulanan)} BOEPD(${int.parse(data.prodBulanan) < int.parse(data.apbn) ? "Belum" : "Sudah"} Tercapai)",
                                statusColor: int.parse(data.prodBulanan) < int.parse(data.apbn) ? Colors.red : Colors.green),
                              CardWidget("Produksi Tahunan : ",
                                "${GlobalFunction.withNumberFormat(data.prodTahunan)} BOEPD(${int.parse(data.prodTahunan) < int.parse(data.apbn) ? "Belum" : "Sudah"} Tercapai)",
                                statusColor: int.parse(data.prodTahunan) < int.parse(data.apbn) ? Colors.red : Colors.green),
                              CardWidget("Target APBN : ","${GlobalFunction.withNumberFormat(data.apbn)} BOEPD",statusColor: Colors.black,),
                            ],
                          );
                        }
                        return Container();
                      }),
                  ],
                )
              ),
              AccordionWidget(4,globalBloc,"LIFTING TAHUN BERJALAN",
                Column(
                  children: <Widget>[
                    // DatePickerWidget(onChange: (DateTime date){
                    //   updateHarianESDMBloc.onChange("lap_pusdatin_lift_tb", date);
                    // }),
                    StreamBuilder(
                      stream: updateHarianESDMBloc.lapPusdatinLiftTbStream,
                      builder: (context, AsyncSnapshot<LapPusdatinLiftTb> snapshot) {
                        if (snapshot.hasData) {
                          var data = snapshot.data;
                          if (["",null].contains(data.idLaporan)) return Container();
                          return Column(
                            children: <Widget>[
                              CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                              CardWidget("Lifting Minyak Bumi : ",data.liftMb),
                              CardWidget("Posisi Stock hari ini : ",data.posisiStock),
                              CardWidget("Saluran Gas : ",data.salurGas),
                            ],
                          );
                        }
                        return Container();
                      }),
                  ],
                )
              ),
              AccordionWidget(5,globalBloc,"HARGA BBM",
                Column(
                  children: <Widget>[
                    // DatePickerWidget(onChange: (DateTime date){
                    //   updateHarianESDMBloc.onChange("lap_pusdatin_harga_bbm", date);
                    // }),
                    StreamBuilder(
                      stream: updateHarianESDMBloc.lapPusdatinHargaBbmStream,
                      builder: (context, AsyncSnapshot<LapPusdatinHargaBbm> snapshot) {
                        if (snapshot.hasData) {
                          var data = snapshot.data;
                          if (["",null].contains(data.idLaporan)) return Container();
                          return Column(
                            children: <Widget>[
                              CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                              CardWidget("Jenis Tertentu : ",data.jenisTertentu),
                              CardWidget("BBM Umum : ",data.bbmUmum),
                              CardWidget("Program Indonesia Satu Harga : ",data.progIndSatuHrg),
                              CardWidget("Harga Per-negara : ",data.hargaPernegara),
                            ],
                          );
                        }
                        return Container();
                      }),
                  ],
                )
              ),
              AccordionWidget(6,globalBloc,"BERITA OPEC HARIAN",
                Column(
                  children: <Widget>[
                    // DatePickerWidget(onChange: (DateTime date){
                    //   updateHarianESDMBloc.onChange("lap_pusdatin_berita_opec_harian", date);
                    // }),
                    StreamBuilder(
                      stream: updateHarianESDMBloc.lapPusdatinBeritaOpecHarianStream,
                      builder: (context, AsyncSnapshot<LapPusdatinBeritaOpecHarian> snapshot) {
                        if (snapshot.hasData) {
                          var data = snapshot.data;
                          if (["",null].contains(data.idLaporan)) return Container();
                          return Column(
                            children: <Widget>[
                              CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                              CardWidget("Berita : ",data.berita),
                              CardWidget("Catatan : ",data.catatan),
                            ],
                          );
                        }
                        return Container();
                      }),
                  ],
                )
              ),
              AccordionWidget(7,globalBloc,"HARGA BATU BARA ACUAN",
                Column(
                  children: <Widget>[
                    // DatePickerWidget(onChange: (DateTime date){
                    //   updateHarianESDMBloc.onChange("lap_pusdatin_harga_bb_acuan", date);
                    // }),
                    StreamBuilder(
                      stream: updateHarianESDMBloc.lapPusdatinHargaBbAcuanStream,
                      builder: (context, AsyncSnapshot<LapPusdatinHargaBbAcuan> snapshot) {
                        if (snapshot.hasData) {
                          var data = snapshot.data;
                          if (["",null].contains(data.idLaporan)) return Container();
                          return Column(
                            children: <Widget>[
                              CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                              CardWidget("Harga : ",data.harga), 
                              CardWidget("Sumber : ",data.sumber),  
                            ],
                          );
                        }
                        return Container();
                      }),
                  ],
                )
              ),
              AccordionWidget(8,globalBloc,"HARGA MINERAL ACUAN",
                Column(
                  children: <Widget>[
                    // DatePickerWidget(onChange: (DateTime date){
                    //   updateHarianESDMBloc.onChange("lap_pusdatin_harga_mineral_acuan", date);
                    // }),
                    StreamBuilder(
                      stream: updateHarianESDMBloc.lapPusdatinHargaMineralAcuanStream,
                      builder: (context, AsyncSnapshot<LapPusdatinHargaMineralAcuan> snapshot) {
                        if (snapshot.hasData) {
                          var data = snapshot.data;
                          if (["",null].contains(data.idLaporan)) return Container();
                          return Column(
                            children: <Widget>[
                              CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                              CardWidget("Tembaga(Cu) : ",data.tembaga+" USD/dmt"),
                              CardWidget("Nikel(Ni) : ",data.nikel+" USD/dmt"),
                              CardWidget("Kobalt(Co) : ",data.kobalt+" USD/dmt"),
                              CardWidget("Alumunium(Al) : ",data.alumunium+" USD/dmt"),
                              CardWidget("Timbal(Pb) : ",data.timbal+" USD/dmt"),
                              CardWidget("Seng(Zn) : ",data.seng+" USD/dmt"),
                              CardWidget("Emas Sebagai Mineral Ikutan : ",data.emas_ikutan+" USD/ounce"),
                              CardWidget("Perak Sebagai Mineral Ikutan : ",data.perak_ikutan+" USD/ounce"),
                              CardWidget("Mangan(Mn) : ",data.mangan+" USD/dmt"),
                              CardWidget("Sumber : ",data.sumber),
                            ],
                          );
                        }
                        return Container();
                      }),
                  ],
                )
              ),
              AccordionWidget(9,globalBloc,"STATUS KETENAGALISTRIKAN",
                Column(
                  children: <Widget>[
                    // DatePickerWidget(onChange: (DateTime date){
                    //   updateHarianESDMBloc.onChange("lap_pusdatin_stts_tl", date);
                    // }),
                    StreamBuilder(
                      stream: updateHarianESDMBloc.lapPusdatinSttsTlStream,
                      builder: (context, AsyncSnapshot<LapPusdatinSttsTl> snapshot) {
                        if (snapshot.hasData) {
                          var data = snapshot.data;
                          if (["",null].contains(data.idLaporan)) return Container();
                          return Column(
                            children: <Widget>[
                              CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                              CardWidget("Catatan : ",data.catatan.toString()),
                              CardWidget("Status Gatrik Jamali : ",data.jamali.toString()),
                              CardWidget("Status Gatrik Non Jamali Sumatera : ",data.jamali_sumatra.toString()),
                              CardWidget("Status Gatrik Non Jamali Indonesia Timur : ",data.jamali_indonesia.toString()),
                            ],
                          );
                        }
                        return Container();
                      }),
                  ],
                )
              ),
              AccordionWidget(10,globalBloc,"CATATAN",
                  Column(
                    children: <Widget>[
                      // DatePickerWidget(onChange: (DateTime date){
                      //   updateHarianESDMBloc.onChange("lap_pusdatin_stts_tl", date);
                      // }),
                      StreamBuilder(
                          stream: updateHarianESDMBloc.lapPusdatinCatatanStream,
                          builder: (context, AsyncSnapshot<LapPusdatinCatatan> snapshot) {
                            if (snapshot.hasData) {
                              var data = snapshot.data;
                              if (["",null].contains(data.idLaporan)) return Container();
                              return Column(
                                children: <Widget>[
                                  CardWidget("Tanggal : ",data.tanggalLaporan.toString()),
                                  CardWidget("Catatan MIGAS : ",data.migas.toString()),
                                  CardWidget("Catatan GATRIK : ",data.gatrik.toString()),
                                ],
                              );
                            }
                            return Container();
                          }),
                    ],
                  )
              ),
            ],
          ),
        ),
        StreamBuilder(
          initialData: false,
          stream: updateHarianESDMBloc.loadingStream,
          builder: (BuildContext ctx, AsyncSnapshot<bool> snapshot) {
            return LoadingWidget(snapshot.data);
          }),
      ],
    );
  }
}