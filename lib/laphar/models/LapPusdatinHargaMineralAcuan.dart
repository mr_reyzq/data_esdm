// To parse this JSON data, do
//
//     final lapPusdatinHargaMineralAcuan = lapPusdatinHargaMineralAcuanFromJson(jsonString);

import 'dart:convert';

LapPusdatinHargaMineralAcuan lapPusdatinHargaMineralAcuanFromJson(String str) => LapPusdatinHargaMineralAcuan.fromJson(json.decode(str));

String lapPusdatinHargaMineralAcuanToJson(LapPusdatinHargaMineralAcuan data) => json.encode(data.toJson());

class LapPusdatinHargaMineralAcuan {
    String idLaporan;
    String tanggalLaporan;
    String harga;
    String sumber;
    String isPost;
    String userEntry;
    String tanggalEntry;
    String flatformEntry;
    String tembaga;
    String nikel;
    String kobalt;
    String alumunium;
    String timbal;
    String seng;
    String emas_ikutan;
    String perak_ikutan;
    String mangan;
    dynamic userPost;
    dynamic tanggalPost;
    dynamic flatformPost;
    dynamic catatanReview;
    dynamic hasReview;
    dynamic tanggalReview;
    dynamic userReview;
    dynamic flatformReview;

    LapPusdatinHargaMineralAcuan({
        this.idLaporan,
        this.tanggalLaporan,
        this.harga,
        this.sumber,
        this.isPost,
        this.userEntry,
        this.tanggalEntry,
        this.flatformEntry,
        this.userPost,
        this.tanggalPost,
        this.flatformPost,
        this.catatanReview,
        this.hasReview,
        this.tanggalReview,
        this.userReview,
        this.flatformReview,
        this.tembaga,
        this.nikel,
        this.kobalt,
        this.alumunium,
        this.timbal,
        this.seng,
        this.emas_ikutan,
        this.perak_ikutan,
        this.mangan,
    });

    factory LapPusdatinHargaMineralAcuan.fromJson(Map<String, dynamic> json) => new LapPusdatinHargaMineralAcuan(
        idLaporan: json["ID_LAPORAN"] ?? "",
        tanggalLaporan: json["TANGGAL_LAPORAN"] ?? "",
        harga: json["HARGA"] ?? "",
        sumber: json["SUMBER"] ?? "",
        isPost: json["IS_POST"] ?? "",
        userEntry: json["USER_ENTRY"] ?? "",
        tanggalEntry: json["TANGGAL_ENTRY"] ?? "",
        flatformEntry: json["FLATFORM_ENTRY"] ?? "",
        userPost: json["USER_POST"] ?? "",
        tanggalPost: json["TANGGAL_POST"] ?? "",
        flatformPost: json["FLATFORM_POST"] ?? "",
        catatanReview: json["CATATAN_REVIEW"] ?? "",
        hasReview: json["HAS_REVIEW"] ?? "",
        tanggalReview: json["TANGGAL_REVIEW"] ?? "",
        userReview: json["USER_REVIEW"] ?? "",
        flatformReview: json["FLATFORM_REVIEW"] ?? "",
        tembaga: json["TEMBAGA"] ?? "",
        nikel: json["NIKEL"] ?? "",
        kobalt: json["KOBALT"] ?? "",
        alumunium: json["ALUMUNIUM"] ?? "",
        timbal: json["TIMBAL"] ?? "",
        seng: json["SENG"] ?? "",
        emas_ikutan: json["EMAS_IKUTAN"] ?? "",
        perak_ikutan: json["PERAK_IKUTAN"] ?? "",
        mangan: json["MANGAN"] ?? "",
    );

    Map<String, dynamic> toJson() => {
        "ID_LAPORAN": idLaporan ?? "",
        "TANGGAL_LAPORAN": tanggalLaporan ?? "",
        "HARGA": harga ?? "",
        "SUMBER": sumber ?? "",
        "IS_POST": isPost ?? "",
        "USER_ENTRY": userEntry ?? "",
        "TANGGAL_ENTRY": tanggalEntry ?? "",
        "FLATFORM_ENTRY": flatformEntry ?? "",
        "USER_POST": userPost ?? "",
        "TANGGAL_POST": tanggalPost ?? "",
        "FLATFORM_POST": flatformPost ?? "",
        "CATATAN_REVIEW": catatanReview ?? "",
        "HAS_REVIEW": hasReview ?? "",
        "TANGGAL_REVIEW": tanggalReview ?? "",
        "USER_REVIEW": userReview ?? "",
        "FLATFORM_REVIEW": flatformReview ?? "",
        "TEMBAGA": tembaga ?? "",
        "NIKEL": nikel ?? "",
        "KOBALT": kobalt ?? "",
        "ALUMUNIUM": alumunium ?? "",
        "TIMBAL": timbal ?? "",
        "SENG": seng ?? "",
        "EMAS_IKUTAN": emas_ikutan ?? "",
        "PERAK_IKUTAN": perak_ikutan ?? "",
        "MANGAN": mangan ?? "",
    };
}
