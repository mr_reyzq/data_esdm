// To parse this JSON data, do
//
//     final lapPusdatinCatatan = lapPusdatinCatatanFromJson(jsonString);

import 'dart:convert';

LapPusdatinCatatan lapPusdatinCatatanFromJson(String str) => LapPusdatinCatatan.fromJson(json.decode(str));

String lapPusdatinCatatanToJson(LapPusdatinCatatan data) => json.encode(data.toJson());

class LapPusdatinCatatan {
  String idLaporan;
  String tanggalLaporan;
  String migas;
  String gatrik;
  dynamic userPost;
  dynamic tanggalPost;
  dynamic flatformPost;
  dynamic catatanReview;
  dynamic hasReview;
  dynamic tanggalReview;
  dynamic userReview;
  dynamic flatformReview;

  LapPusdatinCatatan({
    this.idLaporan,
    this.tanggalLaporan,
    this.migas,
    this.gatrik,
    this.userPost,
    this.tanggalPost,
    this.flatformPost,
    this.catatanReview,
    this.hasReview,
    this.tanggalReview,
    this.userReview,
    this.flatformReview,
  });

  factory LapPusdatinCatatan.fromJson(Map<String, dynamic> json) => new LapPusdatinCatatan(
    idLaporan: json["ID_LAPORAN"] ?? "",
    tanggalLaporan: json["TANGGAL_LAPORAN"] ?? "",
    migas: json["MIGAS"],
    gatrik: json["TENAGA_LISTRIK"],
    userPost: json["USER_POST"] ?? "",
    tanggalPost: json["TANGGAL_POST"] ?? "",
    flatformPost: json["FLATFORM_POST"] ?? "",
    catatanReview: json["CATATAN_REVIEW"] ?? "",
    hasReview: json["HAS_REVIEW"] ?? "",
    tanggalReview: json["TANGGAL_REVIEW"] ?? "",
    userReview: json["USER_REVIEW"] ?? "",
    flatformReview: json["FLATFORM_REVIEW"] ?? "",
  );

  Map<String, dynamic> toJson() => {
    "ID_LAPORAN": idLaporan ?? "",
    "TANGGAL_LAPORAN": tanggalLaporan ?? "",
    "CATATAN": migas ?? "",
    "STATUS": gatrik ?? "",
    "USER_POST": userPost ?? "",
    "TANGGAL_POST": tanggalPost ?? "",
    "FLATFORM_POST": flatformPost ?? "",
    "CATATAN_REVIEW": catatanReview ?? "",
    "HAS_REVIEW": hasReview ?? "",
    "TANGGAL_REVIEW": tanggalReview ?? "",
    "USER_REVIEW": userReview ?? "",
    "FLATFORM_REVIEW": flatformReview ?? "",
  };
}
