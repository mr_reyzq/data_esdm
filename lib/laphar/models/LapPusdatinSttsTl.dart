// To parse this JSON data, do
//
//     final lapPusdatinSttsTl = lapPusdatinSttsTlFromJson(jsonString);

import 'dart:convert';

LapPusdatinSttsTl lapPusdatinSttsTlFromJson(String str) => LapPusdatinSttsTl.fromJson(json.decode(str));

String lapPusdatinSttsTlToJson(LapPusdatinSttsTl data) => json.encode(data.toJson());

class LapPusdatinSttsTl {
    String idLaporan;
    String tanggalLaporan;
    String catatan;
    String status;
    String isPost;
    String userEntry;
    String tanggalEntry;
    String flatformEntry;
    String jamali;
    String jamali_sumatra;
    String jamali_indonesia;
    dynamic userPost;
    dynamic tanggalPost;
    dynamic flatformPost;
    dynamic catatanReview;
    dynamic hasReview;
    dynamic tanggalReview;
    dynamic userReview;
    dynamic flatformReview;

    LapPusdatinSttsTl({
        this.idLaporan,
        this.tanggalLaporan,
        this.status,
        this.isPost,
        this.userEntry,
        this.tanggalEntry,
        this.flatformEntry,
        this.userPost,
        this.tanggalPost,
        this.flatformPost,
        this.catatanReview,
        this.hasReview,
        this.tanggalReview,
        this.catatan,
        this.userReview,
        this.flatformReview,
        this.jamali,
        this.jamali_sumatra,
        this.jamali_indonesia,
    });

    factory LapPusdatinSttsTl.fromJson(Map<String, dynamic> json) => new LapPusdatinSttsTl(
        idLaporan: json["ID_LAPORAN"] ?? "",
        tanggalLaporan: json["TANGGAL_LAPORAN"] ?? "",
        jamali: json["STATUS_GATRIK_JAMALI"],
        jamali_sumatra: json["STATUS_GATRIK_NON_JAMALI_SUMATERA"],
        jamali_indonesia: json["STATUS_GATRIK_NON_JAMALI_INDONESIA_TIMUR"],
        status: json["STATUS"] ?? "",
        isPost: json["IS_POST"] ?? "",
        userEntry: json["USER_ENTRY"] ?? "",
        tanggalEntry: json["TANGGAL_ENTRY"] ?? "",
        catatan: json["CATATAN"],
        flatformEntry: json["FLATFORM_ENTRY"] ?? "",
        userPost: json["USER_POST"] ?? "",
        tanggalPost: json["TANGGAL_POST"] ?? "",
        flatformPost: json["FLATFORM_POST"] ?? "",
        catatanReview: json["CATATAN_REVIEW"] ?? "",
        hasReview: json["HAS_REVIEW"] ?? "",
        tanggalReview: json["TANGGAL_REVIEW"] ?? "",
        userReview: json["USER_REVIEW"] ?? "",
        flatformReview: json["FLATFORM_REVIEW"] ?? "",
    );

    Map<String, dynamic> toJson() => {
        "ID_LAPORAN": idLaporan ?? "",
        "TANGGAL_LAPORAN": tanggalLaporan ?? "",
        "CATATAN": catatan ?? "",
        "STATUS": status ?? "",
        "IS_POST": isPost ?? "",
        "USER_ENTRY": userEntry ?? "",
        "TANGGAL_ENTRY": tanggalEntry ?? "",
        "FLATFORM_ENTRY": flatformEntry ?? "",
        "USER_POST": userPost ?? "",
        "TANGGAL_POST": tanggalPost ?? "",
        "FLATFORM_POST": flatformPost ?? "",
        "CATATAN_REVIEW": catatanReview ?? "",
        "HAS_REVIEW": hasReview ?? "",
        "TANGGAL_REVIEW": tanggalReview ?? "",
        "USER_REVIEW": userReview ?? "",
        "FLATFORM_REVIEW": flatformReview ?? "",
        "STATUS_GATRIK_JAMALI": jamali ?? "",
        "STATUS_GATRIK_NON_JAMALI_SUMATERA": jamali_sumatra ?? "",
        "STATUS_GATRIK_NON_JAMALI_INDONESIA_TIMUR": jamali_indonesia ?? "",
    };
}
